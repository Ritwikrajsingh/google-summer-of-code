<!--
.. title: Idea template
.. slug: idea-template
.. author: Christian Frisson
.. date: 2022-02-16 07:24:11 UTC-05:00
.. tags: ideas, easy, 175 hours
.. type: text
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

...

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

...

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

...

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

...

## Possible mentors

<!-- Please list each mentor hyperlinked to their gitlab profile. 2 possible mentors are more failsafe. -->

[...](https://gitlab.com/)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

...

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

...
